﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CargaArchivos
{
    public partial class Form1 : Form
    {
        private OpenFileDialog _imagenJpg;
        private OpenFileDialog _documentoExcel;
        private OpenFileDialog _documentoPDF;
        private string _ruta;

        public Form1()
        {
            InitializeComponent();
            _imagenJpg = new OpenFileDialog();
            _documentoExcel = new OpenFileDialog();
            _documentoPDF = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\doc\\";

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void btnCargarJpg_Click(object sender, EventArgs e)
        {
            CargarImagenJpg();
        }

        private void CargarImagenJpg()
        {
            _imagenJpg.Filter = "Imagen tipo (*.jpg)|*.jpg";
            _imagenJpg.Title = "Cargar Imagen";
            _imagenJpg.ShowDialog();

            if (_imagenJpg.FileName != "")
            {
                var archivo = new FileInfo(_imagenJpg.FileName);

                txtImagenJpg.Text = archivo.Name;
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarImagenJpg();
        }

        private void GuardarImagenJpg()
        {
            if (_imagenJpg.FileName != null)
            {
                if (_imagenJpg.FileName != "")
                {
                    var archivo = new FileInfo(_imagenJpg.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                        FileInfo archivoAnterior;


                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }

            }
        }

        private void btnEliminarJpg_Click(object sender, EventArgs e)
        {
            EliminarImagenJpg();
        }

        private void EliminarImagenJpg()
        {
            if (MessageBox.Show("Esta seguro de Eliminar esto", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    //codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                    FileInfo archivoAnterior;


                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }

                }

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            GuardarPdf();
        }
        private void GuardarPdf()
        {
            if (_documentoPDF.FileName != null)
            {
                if (_documentoPDF.FileName != "")
                {
                    var archivo = new FileInfo(_documentoPDF.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                        FileInfo archivoAnterior;


                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }

            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CargarExcel();
        }
        private void CargarExcel()
        {
            _documentoExcel.Filter = "Excel tipo (*.xlsx)|*.xlsx";
            _documentoExcel.Title = "Cargar Documento Excel";
            _documentoExcel.ShowDialog();

            if (_documentoExcel.FileName != "")
            {
                var archivo = new FileInfo(_documentoExcel.FileName);

                txtExcel.Text = archivo.Name;
            }

        }

        private void btnEliminarExcel_Click(object sender, EventArgs e)
        {
            EliminarExcel();
        }
        private void EliminarExcel()
        {
            if (MessageBox.Show("Esta seguro de Eliminar esto", "Eliminar Documento Excel", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    //codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.xlsx");
                    FileInfo archivoAnterior;


                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                        }
                    }
                }
            }
        }

        private void btnGuardarExcel_Click(object sender, EventArgs e)
        {
            GuardarExcel();
        }
        private void GuardarExcel()
        {
            if (_documentoExcel.FileName != null)
            {
                if (_documentoExcel.FileName != "")
                {
                    var archivo = new FileInfo(_documentoExcel.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        //codigo para agregar el archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.xlsx");
                        FileInfo archivoAnterior;


                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }

            }
        }

        private void btnCargarPDF_Click(object sender, EventArgs e)
        {
            CargarPDF();
        }
        private void CargarPDF()
        {
            _documentoPDF.Filter = "Documento PDF tipo (*.pdf)|*.pdf";
            _documentoPDF.Title = "Cargar Documento PDF";
            _documentoPDF.ShowDialog();

            if (_documentoPDF.FileName != "")
            {
                var archivo = new FileInfo(_documentoPDF.FileName);

                txtPDF.Text = archivo.Name;
            }
        }

        private void btnEliminarPDF_Click(object sender, EventArgs e)
        {
            EliminarPDF();
        }
        private void EliminarPDF()
        {
            if (MessageBox.Show("Esta seguro de Eliminar esto", "Eliminar PDF", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    //codigo para agregar el archivo

                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                    FileInfo archivoAnterior;


                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }
                    }

                }

            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void button10_Click_1(object sender, EventArgs e)
        {

        }
    }
}
