﻿namespace CargaArchivos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtImagenJpg = new System.Windows.Forms.TextBox();
            this.btnCargarJpg = new System.Windows.Forms.Button();
            this.btnEliminarJpg = new System.Windows.Forms.Button();
            this.btnGuardarI = new System.Windows.Forms.Button();
            this.btnGuardarPDF = new System.Windows.Forms.Button();
            this.btnGuardarExcel = new System.Windows.Forms.Button();
            this.btnEliminarExcel = new System.Windows.Forms.Button();
            this.btnCargarExcel = new System.Windows.Forms.Button();
            this.txtExcel = new System.Windows.Forms.TextBox();
            this.btnEliminarPDF = new System.Windows.Forms.Button();
            this.btnCargarPDF = new System.Windows.Forms.Button();
            this.txtPDF = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtImagenJpg
            // 
            this.txtImagenJpg.Location = new System.Drawing.Point(22, 35);
            this.txtImagenJpg.Name = "txtImagenJpg";
            this.txtImagenJpg.Size = new System.Drawing.Size(261, 25);
            this.txtImagenJpg.TabIndex = 0;
            this.txtImagenJpg.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnCargarJpg
            // 
            this.btnCargarJpg.Location = new System.Drawing.Point(289, 35);
            this.btnCargarJpg.Name = "btnCargarJpg";
            this.btnCargarJpg.Size = new System.Drawing.Size(48, 23);
            this.btnCargarJpg.TabIndex = 1;
            this.btnCargarJpg.Text = "...";
            this.btnCargarJpg.UseVisualStyleBackColor = true;
            this.btnCargarJpg.Click += new System.EventHandler(this.btnCargarJpg_Click);
            // 
            // btnEliminarJpg
            // 
            this.btnEliminarJpg.Location = new System.Drawing.Point(343, 35);
            this.btnEliminarJpg.Name = "btnEliminarJpg";
            this.btnEliminarJpg.Size = new System.Drawing.Size(42, 23);
            this.btnEliminarJpg.TabIndex = 2;
            this.btnEliminarJpg.Text = "x";
            this.btnEliminarJpg.UseVisualStyleBackColor = true;
            this.btnEliminarJpg.Click += new System.EventHandler(this.btnEliminarJpg_Click);
            // 
            // btnGuardarI
            // 
            this.btnGuardarI.Location = new System.Drawing.Point(289, 76);
            this.btnGuardarI.Name = "btnGuardarI";
            this.btnGuardarI.Size = new System.Drawing.Size(96, 32);
            this.btnGuardarI.TabIndex = 3;
            this.btnGuardarI.Text = "Guardar";
            this.btnGuardarI.UseVisualStyleBackColor = true;
            this.btnGuardarI.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnGuardarPDF
            // 
            this.btnGuardarPDF.Location = new System.Drawing.Point(289, 77);
            this.btnGuardarPDF.Name = "btnGuardarPDF";
            this.btnGuardarPDF.Size = new System.Drawing.Size(96, 34);
            this.btnGuardarPDF.TabIndex = 4;
            this.btnGuardarPDF.Text = "Guardar";
            this.btnGuardarPDF.UseVisualStyleBackColor = true;
            this.btnGuardarPDF.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnGuardarExcel
            // 
            this.btnGuardarExcel.Location = new System.Drawing.Point(289, 77);
            this.btnGuardarExcel.Name = "btnGuardarExcel";
            this.btnGuardarExcel.Size = new System.Drawing.Size(96, 34);
            this.btnGuardarExcel.TabIndex = 5;
            this.btnGuardarExcel.Text = "Guardar ";
            this.btnGuardarExcel.UseVisualStyleBackColor = true;
            this.btnGuardarExcel.Click += new System.EventHandler(this.btnGuardarExcel_Click);
            // 
            // btnEliminarExcel
            // 
            this.btnEliminarExcel.Location = new System.Drawing.Point(343, 33);
            this.btnEliminarExcel.Name = "btnEliminarExcel";
            this.btnEliminarExcel.Size = new System.Drawing.Size(42, 23);
            this.btnEliminarExcel.TabIndex = 8;
            this.btnEliminarExcel.Text = "x";
            this.btnEliminarExcel.UseVisualStyleBackColor = true;
            this.btnEliminarExcel.Click += new System.EventHandler(this.btnEliminarExcel_Click);
            // 
            // btnCargarExcel
            // 
            this.btnCargarExcel.Location = new System.Drawing.Point(289, 33);
            this.btnCargarExcel.Name = "btnCargarExcel";
            this.btnCargarExcel.Size = new System.Drawing.Size(48, 23);
            this.btnCargarExcel.TabIndex = 7;
            this.btnCargarExcel.Text = "...";
            this.btnCargarExcel.UseVisualStyleBackColor = true;
            this.btnCargarExcel.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtExcel
            // 
            this.txtExcel.Location = new System.Drawing.Point(22, 33);
            this.txtExcel.Name = "txtExcel";
            this.txtExcel.Size = new System.Drawing.Size(261, 25);
            this.txtExcel.TabIndex = 6;
            // 
            // btnEliminarPDF
            // 
            this.btnEliminarPDF.Location = new System.Drawing.Point(343, 34);
            this.btnEliminarPDF.Name = "btnEliminarPDF";
            this.btnEliminarPDF.Size = new System.Drawing.Size(42, 23);
            this.btnEliminarPDF.TabIndex = 11;
            this.btnEliminarPDF.Text = "x";
            this.btnEliminarPDF.UseVisualStyleBackColor = true;
            this.btnEliminarPDF.Click += new System.EventHandler(this.btnEliminarPDF_Click);
            // 
            // btnCargarPDF
            // 
            this.btnCargarPDF.Location = new System.Drawing.Point(289, 34);
            this.btnCargarPDF.Name = "btnCargarPDF";
            this.btnCargarPDF.Size = new System.Drawing.Size(48, 23);
            this.btnCargarPDF.TabIndex = 10;
            this.btnCargarPDF.Text = "...";
            this.btnCargarPDF.UseVisualStyleBackColor = true;
            this.btnCargarPDF.Click += new System.EventHandler(this.btnCargarPDF_Click);
            // 
            // txtPDF
            // 
            this.txtPDF.Location = new System.Drawing.Point(22, 34);
            this.txtPDF.Name = "txtPDF";
            this.txtPDF.Size = new System.Drawing.Size(261, 25);
            this.txtPDF.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtImagenJpg);
            this.groupBox1.Controls.Add(this.btnCargarJpg);
            this.groupBox1.Controls.Add(this.btnEliminarJpg);
            this.groupBox1.Controls.Add(this.btnGuardarI);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 132);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Imagenes";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtExcel);
            this.groupBox2.Controls.Add(this.btnCargarExcel);
            this.groupBox2.Controls.Add(this.btnGuardarExcel);
            this.groupBox2.Controls.Add(this.btnEliminarExcel);
            this.groupBox2.Location = new System.Drawing.Point(12, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(404, 132);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Documentos Excel";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtPDF);
            this.groupBox3.Controls.Add(this.btnGuardarPDF);
            this.groupBox3.Controls.Add(this.btnCargarPDF);
            this.groupBox3.Controls.Add(this.btnEliminarPDF);
            this.groupBox3.Location = new System.Drawing.Point(12, 318);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(404, 132);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Documentos PDF";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 462);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtImagenJpg;
        private System.Windows.Forms.Button btnCargarJpg;
        private System.Windows.Forms.Button btnEliminarJpg;
        private System.Windows.Forms.Button btnGuardarI;
        private System.Windows.Forms.Button btnGuardarPDF;
        private System.Windows.Forms.Button btnGuardarExcel;
        private System.Windows.Forms.Button btnEliminarExcel;
        private System.Windows.Forms.Button btnCargarExcel;
        private System.Windows.Forms.TextBox txtExcel;
        private System.Windows.Forms.Button btnEliminarPDF;
        private System.Windows.Forms.Button btnCargarPDF;
        private System.Windows.Forms.TextBox txtPDF;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

